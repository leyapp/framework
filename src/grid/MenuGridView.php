<?php

namespace leyapp\framework\grid;


use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class MenuGridView extends Widget
{
	public $itemClass = 'leyapp\framework\grid\MenuGridViewItem';
	public $items;

	public function run()
	{
		return Html::tag('div', $this->renderItems(), ['class' => 'row']);
	}

	public function renderItems()
	{
		$itemsRendered = '';

		foreach ($this->items as $item) {
			$config     = ArrayHelper::merge(['class' => $this->itemClass], $item);
			$itemObject = \Yii::createObject($config);

			$itemsRendered .= $itemObject->render();
		}

		return $itemsRendered;
	}
}