<?php

namespace leyapp\framework\grid;


use yii\base\BaseObject;
use yii\helpers\Html;

class MenuGridViewItem extends BaseObject
{
	public $title;
	public $description;
	public $url;
	public $requiredPermission = null;
	public $tag                = 'div';
	public $htmlOptions        = ['class' => 'col-md-4 col-sm-6 col-xs-12'];

	public function render()
	{
		return Html::tag($this->tag, $this->renderItemContent(), $this->htmlOptions);
	}

	public function renderItemContent()
	{
		return Html::tag('h2', $this->title)
		       . Html::tag('p', $this->description)
		       . Html::tag('p', Html::a('Administrar', $this->url, ['class' => 'btn btn-default']));;
	}
}