<?php

namespace leyapp\framework\interfaces;


interface LeyappModuleInterface
{
	public function getName();

	public function getDescription();

	public static function getDbConnectionName();

	public function requiredEnvVars();
}
