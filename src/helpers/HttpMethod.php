<?php

namespace leyapp\framework\helpers;

class HttpMethod
{
	public const GET     = 'GET';
	public const POST    = 'POST';
	public const PUT     = 'PUT';
	public const PATCH   = 'PATCH';
	public const DELETE  = 'DELETE';
	public const OPTIONS = 'OPTIONS';
}