<?php

namespace leyapp\framework\components\aws;


use Yii;
use yii\base\InvalidConfigException;

class S3 extends Amazon implements AWSInterface
{
	/**
	 * For more information about ACL required permissions
	 *
	 * @see https://docs.aws.amazon.com/AmazonS3/latest/dev/acl-overview.html#canned-acl
	 */
	const ACL_PRIVATE            = 'private';
	const ACL_PUBLIC_READ        = 'public-read';
	const ACL_PUBLIC_READ_WRITE  = 'public-read-write';
	const ACL_AUTHENTICATED_READ = 'authenticated-read';

	/**
	 * For more information about Storage Classes
	 *
	 * @see https://docs.aws.amazon.com/AmazonS3/latest/dev/storage-class-intro.html
	 */
	const STORAGE_CLASS_STANDARD           = 'STANDARD';
	const STORAGE_CLASS_REDUCED_REDUNDANCY = 'REDUCED_REDUNDANCY';
	const STORAGE_CLASS_GLACIER            = 'GLACIER';

	/**
	 * @var string $bucketName
	 */
	private $bucketName;

	/**
	 * @var \Aws\Result $uploadedFile
	 */
	public $uploadedFile;

	public static $bucketBaseUrl;
	public static $cdnBaseUrl;

	/**
	 * S3 constructor.
	 * @throws \yii\base\InvalidConfigException
	 */
	public function __construct()
	{
		parent::__construct();

		$params = Yii::$app->params;

		$this->validateParams($params);

		self::$bucketBaseUrl = "https://s3.{$this->region}.amazonaws.com/{$this->bucketName}";
	}

	/**
	 * @param null $params
	 *
	 * @throws InvalidConfigException
	 */
	public function validateParams($params = null)
	{
		parent::validateParams($params);

		$params = is_null($params) ? Yii::$app->params : $params;

		if ( ! isset($params['amazon']['s3'])) {
			throw new InvalidConfigException('Configuration file for Amazon S3 were not found');
		} else {
			$s3Params = $params['amazon']['s3'];
		}

		if ( ! isset($s3Params['bucketName'])) {
			throw new InvalidConfigException('Configuration file for Amazon S3 Bucket Name were not found');
		} else {
			$this->bucketName = $s3Params['bucketName'];
		}

		self::$cdnBaseUrl = isset($s3Params['cdnBaseUrl']) ? $s3Params['cdnBaseUrl'] : null;
	}

	/**
	 * Upload an User's profile picture to Amazon Bucket, additionally you can get the CDN url by using the
	 * getCDNUploadedFileUrl() or get the bucket Url by using getUploadedFileUrl()
	 *
	 * usage Example:
	 *
	 * ```php
	 * $sourcePath = '/path/to/your/file.png';
	 * $keyName    = 'path/in/bucket/to/file.png';
	 *
	 * return Amazon::s3()
	 *              ->uploadFile($sourcePath, $keyName)
	 *              ->getCDNUploadedFileUrl();
	 * ```
	 *
	 * @param $sourcePath
	 * @param $keyName
	 *
	 * @return $this
	 */
	public function uploadFile($sourcePath, $keyName)
	{
		$this->uploadedFile = $this->getSDK()
		                           ->createS3()
		                           ->putObject([
			                           'Bucket'       => $this->bucketName,
			                           'Key'          => $keyName,
			                           'SourceFile'   => $sourcePath,
			                           'ACL'          => self::ACL_PUBLIC_READ,
			                           'StorageClass' => self::STORAGE_CLASS_STANDARD,
		                           ]);

		return $this;
	}

	/**
	 * @return string|null
	 */
	public function getUploadedFileUrl()
	{
		return $this->uploadedFile->get('ObjectURL');
	}

	/**
	 * @return string|array|null
	 * @throws InvalidConfigException
	 */
	public function getCDNUploadedFileUrl()
	{
		if (is_null(self::$cdnBaseUrl)) {
			throw new InvalidConfigException('Configuration property for CDN Url were not found, instead use Amazon::getUploadedFileUrl($uploadResult)');
		}

		$result = str_replace(self::$bucketBaseUrl, self::$cdnBaseUrl, $this->uploadedFile->get('ObjectURL'));

		return is_array($result) ? reset($result) : $result;
	}

	public function getObject($bucket, $key)
	{
		return $this->getSDK()
		            ->createS3()
		            ->getObject([
			            'Bucket' => $bucket,
			            'Key'    => $key
		            ]);
	}

	public function deleteObject($bucket, $key)
	{
		return $this->getSDK()
		            ->createS3()
		            ->deleteObject([
			            'Bucket' => $bucket,
			            'Key'    => $key
		            ]);
	}
}