<?php

namespace leyapp\framework\components\aws;


interface AWSInterface
{
	function validateParams($params = null);
}