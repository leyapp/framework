<?php

namespace leyapp\framework\components\aws;

use Yii;
use Aws\Sdk;
use Aws\Credentials\Credentials;
use yii\base\InvalidConfigException;

class Amazon implements AWSInterface
{
	private $key;
	private $secret;

	public $region;

	/**
	 * @var Sdk $sdk
	 */
	public $sdk;

	/**
	 * Amazon constructor.
	 * @throws InvalidConfigException
	 */
	public function __construct()
	{
		$params = Yii::$app->params;

		$this->validateParams($params);
	}

	/**
	 * @param null $params
	 *
	 * @throws InvalidConfigException
	 */
	public function validateParams($params = null)
	{
		$params = is_null($params) ? Yii::$app->params : $params;

		if ( ! isset($params['amazon'])) {
			throw new InvalidConfigException('Configuration file for Amazon AWS were not found');
		}

		if ( ! isset($params['amazon']['key'])) {
			throw new InvalidConfigException('Configuration file for Amazon S3 Key were not found');
		} else {
			$this->key = $params['amazon']['key'];
		}

		if ( ! isset($params['amazon']['secret'])) {
			throw new InvalidConfigException('Configuration file for Amazon S3 Secret were not found');
		} else {
			$this->secret = $params['amazon']['secret'];
		}

		if ( ! isset($params['amazon']['region'])) {
			throw new InvalidConfigException('Configuration file for Amazon S3 Region were not found');
		} else {
			$this->region = $params['amazon']['region'];
		}
	}

	public function createCredentials()
	{
		return new Credentials($this->key, $this->secret);
	}

	public function getSDK()
	{
		$config = [
			'region'      => $this->region,
			'version'     => 'latest',
			'credentials' => $this->createCredentials()
		];

		return new Sdk($config);
	}

	/**
	 * @return S3
	 * @throws InvalidConfigException
	 */
	public static function s3()
	{
		return new S3();
	}
}