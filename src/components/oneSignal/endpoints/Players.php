<?php

namespace leyapp\framework\components\oneSignal\endpoints;


use leyapp\framework\components\oneSignal\endpoints\base\OneSignalClient;
use leyapp\framework\helpers\HttpCode;

class Players extends OneSignalClient
{
	public $endpoint             = '/players';
	public $request_endpoint_url = null;

	public function init()
	{
		parent::init();

		$this->request_endpoint_url = $this->base_url . $this->version . $this->endpoint;
	}

	public function getPlayer($id)
	{
		$request = $this->get($this->request_endpoint_url . '/' . $id)
		                ->send();

		if ($request->statusCode == HttpCode::OK) {
			return $request->data;
		} else {
			\Yii::$app->response->setStatusCode(HttpCode::UNPROCESSABLE_ENTITY);

			return $request->data;
		}
	}
}