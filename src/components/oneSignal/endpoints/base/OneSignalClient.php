<?php

namespace leyapp\framework\components\oneSignal\endpoints\base;


use yii\base\Exception;
use yii\base\InvalidConfigException;
use yii\httpclient\Client;
use yii\httpclient\CurlTransport;

class OneSignalClient extends Client
{
	const CLIENT_TYPE_IOS                = 0;
	const CLIENT_TYPE_ANDROID            = 1;
	const CLIENT_TYPE_AMAZON             = 2;
	const CLIENT_TYPE_WINDOWS_PHONE_MPNS = 3;
	const CLIENT_TYPE_CHROME_APP         = 4;
	const CLIENT_TYPE_CHROME_WEBSITE     = 5;
	const CLIENT_TYPE_WINDOWS_PHONE_WNS  = 6;
	const CLIENT_TYPE_SAFARI             = 7;
	const CLIENT_TYPE_FIREFOX            = 8;
	const CLIENT_TYPE_MAC_OSX            = 8;


	/**
	 * @var string|null $app_id
	 */
	public $app_id = null;

	/**
	 * @var string|null $rest_api_key
	 */
	public $rest_api_key = null;

	/**
	 * @var string $base_url
	 */
	public $base_url = 'https://onesignal.com/api/';

	/**
	 * @var string $version
	 */
	public $version = 'v1';

	/**
	 * @var string|null
	 */
	public $endpoint = null;

	/**
	 * @throws Exception
	 * @throws InvalidConfigException
	 */
	public function init()
	{
		parent::init();

		if ( ! $this->app_id = getenv('ONE_SIGNAL_APP_ID')) {
			throw new InvalidConfigException('Necesita configurar la variable de entorno "ONE_SIGNAL_APP_ID"');
		}

		if ( ! $this->rest_api_key = getenv('ONE_SIGNAL_API_KEY')) {
			throw new InvalidConfigException('Necesita configurar la variable de entorno "ONE_SIGNAL_API_KEY"');
		}

		if (is_null($this->endpoint)) {
			throw new InvalidConfigException('debe configurar un Endpoint para hacer la solicitud');
		}

		$this->setTransport(CurlTransport::className());
	}
}