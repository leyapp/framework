<?php

namespace leyapp\framework\components\oneSignal\endpoints;

use leyapp\framework\components\oneSignal\endpoints\base\OneSignalClient;
use leyapp\framework\helpers\HttpCode;
use yii\helpers\ArrayHelper;
use yii\httpclient\Response;

class Notifications extends OneSignalClient
{
	public $endpoint             = '/notifications';
	public $request_endpoint_url = null;

	public function init()
	{
		parent::init();

		$this->request_endpoint_url = $this->base_url . $this->version . $this->endpoint;
	}

	/**
	 * @param $user_id
	 * @param $role_id
	 * @param $data
	 *
	 * @return bool|\yii\httpclient\Response
	 */
	public function notifyUsers($user_id, $role_id, $data)
	{
		$data['app_id']  = $this->app_id;
		$data['filters'] = [
			self::addFilterCondition('tag', 'id', '=', $user_id),
			self::addOperator('AND'),
			self::addFilterCondition('tag', 'role', '=', $role_id)
		];

		return $this->send($data);
	}

	/**
	 * @param array $data
	 *
	 * @return bool|\yii\httpclient\Response
	 */
	public function send($data)
	{
		$response = $this->post($this->request_endpoint_url, json_encode($data), [
			'Authorization' => 'Basic ' . $this->rest_api_key,
			'Content-Type'  => 'application/json'
		])
		                 ->setFormat(self::FORMAT_RAW_URLENCODED)
		                 ->send();

		return $this->processResponse($response);
	}

	/**
	 * @param String $segment
	 * @param array  $data
	 *
	 * @return bool|\yii\httpclient\Response
	 */
	public function notifySegment($segment, $data)
	{
		$data = ArrayHelper::merge($data, [
			'app_id'  => $this->app_id,
			'filters' => [
				self::addFilterCondition('tag', 'type', '=', $segment)
			]
		]);

		return $this->send($data);
	}

	/**
	 * @param $response
	 *
	 * @return bool
	 */
	public function processResponse(Response $response)
	{
		if ($response->statusCode == HttpCode::OK) {
			return true;
		} else {
			\Yii::error('Error al enviar push: \n' . $response->toString());

			return false;
		}
	}

	/**
	 * @param String $field
	 * @param String $key
	 * @param String $relation
	 * @param String $value
	 *
	 * @return array
	 */
	public static function addFilterCondition($field, $key, $relation, $value)
	{
		return ['field' => $field, 'key' => $key, 'relation' => $relation, 'value' => $value];
	}

	/**
	 * @param String $operator
	 *
	 * @return array
	 */
	public static function addOperator($operator)
	{
		return ['operator' => $operator];
	}
}