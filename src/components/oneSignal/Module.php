<?php

namespace leyapp\framework\components\oneSignal;

use leyapp\framework\components\oneSignal\Endpoints\Base\OneSignalClient;
use leyapp\framework\components\oneSignal\Endpoints\Notifications;
use leyapp\framework\components\oneSignal\Endpoints\Players;
use yii\base\InvalidConfigException;
use yii\helpers\VarDumper;
use yii\httpclient\Client;

class Module extends \yii\base\Module
{
	/**
	 * @var string|null $app_id
	 */
	public $app_id = null;

	/**
	 * @var string|null $rest_api_key
	 */
	public $rest_api_key = null;

	/**
	 * @var string $base_url
	 */
	public $base_url = 'https://onesignal.com/api/';

	/**
	 * @var string $version
	 */
	public $version = 'v1';

	/**
	 * @throws InvalidConfigException
	 */
	public function init()
	{
		parent::init();

		if (is_null($this->app_id)) {
			throw new InvalidConfigException('Debe configurar un App ID para OneSignal');
		}

		if (is_null($this->rest_api_key)) {
			throw new InvalidConfigException('Debe configurar un Rest API Key para OneSignal');
		}
	}

	/**
	 * Get an OneSignal API instance
	 *
	 * @return Client
	 */
	public function api()
	{
		$client = new Client;

		$response = $client->create([
			'app_id'      => $this->app_id,
			'language'    => 'es',
			'device_type' => $client::CLIENT_TYPE_ANDROID,
			'tags'        => ['device', 'type']
		]);
	}

	/**
	 * @return \leyapp\framework\components\oneSignal\Endpoints\Players
	 */
	public function players()
	{
		return $this->setClientBasics((new Players()));
	}

	/**
	 * @return \leyapp\framework\components\oneSignal\Endpoints\Notifications
	 */
	public function notifications()
	{
		return $this->setClientBasics((new Notifications()));
	}

	protected function setClientBasics($client)
	{
		$client->app_id       = $this->app_id;
		$client->rest_api_key = $this->rest_api_key;

		return $client;
	}
}