<?php

namespace leyapp\framework\jobs;

use yii\base\BaseObject;
use yii\queue\JobInterface;

class EmailJob extends BaseObject implements JobInterface
{
	public $subject;
	public $name;
	public $email;
	public $body;
	public $cc;

	public function execute($queue)
	{
		return false;
	}
}